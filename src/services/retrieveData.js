const baseUrl = process.env.VUE_APP_BASE_URL;
const dataUser = localStorage.getItem("dataUser");
const dataUserParse = JSON.parse(dataUser);

export const PostLogin = (data) => {
  const res = fetch(`${baseUrl}/auth/login`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(data),
  });

  return res;
};

export const PostRegister = (data) => {
  const res = fetch(`${baseUrl}/auth/register`, {
    method: "POST",
    headers: {
      "Content-type": "application/json",
    },
    body: JSON.stringify(data),
  });

  return res;
};

export const PutUsers = (data, id) => {
  const res = fetch(`${baseUrl}/api/users/${id}`, {
    method: "PATCH",
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${dataUserParse.token}`,
    },
    body: JSON.stringify(data),
  });

  return res;
};

export const getListUsers = () => {
  const res = fetch(`${baseUrl}/api/users`, {
    method: "GET",
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${dataUserParse.token}`,
    },
  });

  return res;
};

export const deleteUser = (id) => {
  const res = fetch(`${baseUrl}/api/users/${id}`, {
    method: "DELETE",
    headers: {
      "Content-type": "application/json",
      Authorization: `Bearer ${dataUserParse.token}`,
    },
  });

  return res;
};
